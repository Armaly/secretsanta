package armalySecretSanta;

public class Participant {
	private String name; //Username
	private String steamID; //Steam ID
	private String address1; //Line 1 of address
	private String address2; //Line 2 of address
	private Participant gifter; //Person user is gifting to
	private Participant giftee; //Person user is receiving a gift from
	private boolean hasReceived; // Whether or not user has received a gift
	private boolean hasGifted; //Whether the user has gifted their giftee or not
	
	//Constructors
	public Participant()
	{
		name = "";
		steamID = "";
		gifter = null;
		giftee = null;
		hasReceived = false;
		hasGifted = false; 
	}
	public Participant(String name)
	{
		this.name = name;
		steamID = "";
		gifter = null;
		giftee = null;
		hasReceived = false;
		hasGifted = false;
	}
	//Getters and Setters
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getSteamID()
	{
		return steamID;
	}
	public void setSteamID(String steamID)
	{
		this.steamID = steamID;
	}
	public Participant getGifter()
	{
		return gifter;
	}
	public void setGifter(Participant gifter)
	{
		this.gifter = gifter;
	}
	public Participant getGiftee()
	{
		return giftee;
	}
	public String getAdress1()
	{
		return address1;
	}
	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}
	public String getAddress2()
	{
		return address2;
	}
	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}
	public void setGiftee(Participant giftee)
	{
		this.giftee = giftee;
	}
	public boolean getReceived()
	{
		return hasReceived;
	}
	public void setReceived(boolean hasReceived)
	{
		this.hasReceived = hasReceived;
	}
	public boolean getGifted()
	{
		return hasGifted;
	}
	public void setGifted(boolean hasGifted)
	{
		this.hasGifted = hasGifted;
	}

}

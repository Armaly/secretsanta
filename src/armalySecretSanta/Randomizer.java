package armalySecretSanta;

import java.util.ArrayList;
import java.util.Scanner;

public class Randomizer {

	//Preconditions: Is ran
	//Postconditions: Adds a participant with a specified name and steam id to end of array list
	public static void addParticipant(ArrayList<Participant> santaList)
	{
		Participant temp = new Participant();
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter the name.");
		String name = input.nextLine();
	/*	System.out.println("Please enter the steam id.");
		String steam = input.nextLine(); */
		
		temp.setName(name);
	//	temp.setSteamID(steam);
		
		santaList.add(temp);
		
		System.out.println(name+" has been added to the list!");
		
		//input.close();
	}
	public static void addParticipant(ArrayList<Participant> santaList, String[] name)
	{
		for(int i=0;i<name.length;i++)
		{
			Participant temp = new Participant();
			temp.setName(name[i]);
			santaList.add(temp);
		
			System.out.println(name[i]+" has been added to the list!");
		}
		
	}
	//Preconditions: Is ran
	//Postconditions: Either finds the participants and removes from list or outputs failure message
	public static void removeParticipant(ArrayList<Participant> santaList)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the name of the participant you would like to remove.");
		String name = input.nextLine();
		boolean remove = false;
		
		for(int i=0;i<santaList.size();i++)
		{
			if(santaList.get(i).getName().equals(name))
			{
				santaList.remove(i);
				remove = true;
			}
		}
		
		if(remove==true)
		{
			System.out.println("Removal Successful!");
		}
		else
		{
			System.out.println("Removal failed.");
		}
		
		input.close();
	}
	//Preconditions: Is ran
	//Postconditions: Pairs each gifter to a unique giftee
	public static void randomize(ArrayList<Participant> santaList)
	{
		int count = santaList.size();
		
		if(count<=1)
		{
			System.out.println("Error, not enough participants to scramble.");
		}
		else
		{
			for(int i=0; i<santaList.size();i++)
			{
				int temp = (int) Math.random() * santaList.size()+1;
				
				//If they do not have a gifter already and the giftee is not their gifter 
				if(santaList.get(temp).getGifter()==null && santaList.get(i).getGiftee().getName()!=santaList.get(temp).getName())
				{
					santaList.get(temp).setGifter(santaList.get(i));
					santaList.get(i).setGiftee(santaList.get(temp));
				}
				else
				{
					while(santaList.get(temp).getGifter()!=null && santaList.get(i).getGiftee().getName()!=santaList.get(temp).getName())
					{
						temp = (int) Math.random() * santaList.size()+1;
					}
					santaList.get(temp).setGifter(santaList.get(i));
					santaList.get(i).setGiftee(santaList.get(temp));
				}
			}
		}	
	}
	
	//Preconditions: Is called
	//Postconditions: Prints out the full list along with every detail of each participant
	public static void printFullList(ArrayList<Participant> santaList)
	{
		for(int i=0;i<santaList.size();i++)
		{
			System.out.println("Name "+santaList.get(i).getName()+" SteamID: "+santaList.get(i).getSteamID()+" Gifter: "+santaList.get(i).getGifter()+" Giftee: "+ santaList.get(i).getGiftee() +" Address 1: "+ santaList.get(i).getAdress1() + " Address 2: "+santaList.get(i).getAddress2());
		}
	}
	//Preconditions: Is called
	//Postconditions: Prints out a short detail list of the participants
	public static void printShortList(ArrayList<Participant> santaList)
	{
		for(int i=0;i<santaList.size();i++)
		{
			System.out.println("Name "+santaList.get(i).getName()+" Gifter: "+santaList.get(i).getGifter()+" Giftee: "+ santaList.get(i).getGiftee() );
		}
	}
	//Preconditions: Is called
	//Postconditions: Returns a list of those who have not sent a gift
	public static ArrayList<Participant>getUngifted(ArrayList<Participant> santaList)
	{
		ArrayList<Participant> temp = new ArrayList<>();
		
		for(int i=0;i<santaList.size();i++)
		{
			if(santaList.get(i).getGifted()==false)
			{
				temp.add(santaList.get(i));
			}
		}
		
		return temp;
	}
	//Preconditions: Is called
	//Postconditions: Returns a list of those who have not received a gift
	public static ArrayList<Participant>getUnreceived(ArrayList<Participant> santaList)
	{
		ArrayList<Participant> temp = new ArrayList<>();
		
		for(int i=0;i<santaList.size();i++)
		{
			if(santaList.get(i).getReceived()==false)
			{
				temp.add(santaList.get(i));
			}
		}
		
		return temp;
	}
	//Preconditions: Is called
	//Postconditions: Erases all elements in the list 
	public static void clear(ArrayList<Participant> santaList)
	{
		santaList.clear();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<Participant> santaList = new ArrayList<>();
		
		String[] name = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
		
		
		
		addParticipant(santaList, name);

		printShortList(santaList);
		
		//removeParticipant(santaList);
		//randomize(santaList);
		
		printShortList(santaList);
		

	}

}
